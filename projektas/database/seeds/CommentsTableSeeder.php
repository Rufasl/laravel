<?php

use Illuminate\Database\Seeder;
use App\Comment as Comment;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Comment::truncate();

        Comment::create( [
            'inventory_id' => 1 ,
            'comment' => "Sugedes kompiuteris" ,
            'user_id' => '2'
        ] );

        Comment::create( [
            'inventory_id' => 2 ,
            'comment' => "Stalas kleba" ,
            'user_id' => '3'
        ] );
    }
}
