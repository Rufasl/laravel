<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(SupervisorsTableSeeder::class);
        $this->call(InventoriesTableSeeder::class);
        $this->call(RoomsTableSeeder::class);
        $this->call(Inventory_typesTableSeeder::class);
        $this->call(CommentsTableSeeder::class);
        $this->call(UsersTableSeeder::class);

        Model::reguard();

    }
}
