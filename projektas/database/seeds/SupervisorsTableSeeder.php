<?php

use Illuminate\Database\Seeder;
use App\Supervisor as Supervisor;

class SupervisorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Supervisor::truncate();

        Supervisor::create( [
            'user_id' => '2' ,
            'inventory_id' => '1' ,
            'name' => 'Karolis',
            'surname' => 'Banelis',
            'date_from' => '2015-11-03' ,
            'date_to' => '2015-12-03'
        ] );

        Supervisor::create( [
            'user_id' => '1' ,
            'inventory_id' => '2' ,
            'name' => 'Ignotas',
            'surname' => 'Pancerevas',
            'date_from' => '2015-11-07' ,
            'date_to' => '2015-12-07'
        ] );
    }

}
