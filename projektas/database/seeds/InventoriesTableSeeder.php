<?php

use Illuminate\Database\Seeder;
use App\Inventory as Inventory;

class InventoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Inventory::truncate();

        Inventory::create( [
            'room_id' => 1 ,
            'inventory_type_id' => 1 ,
            'purchase_date' => '2014-01-05' ,
            'price' => 39.99 ,
            'barcode' => 789456123
        ] );

        Inventory::create( [
            'room_id' => 2 ,
            'inventory_type_id' => 2 ,
            'purchase_date' => '2014-03-12' ,
            'price' => 899.99,
            'barcode' => 456123897,
        ] );
    }
}
