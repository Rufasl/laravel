<?php

use Illuminate\Database\Seeder;
use App\Room as Room;

class RoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Room::truncate();

        Room::create( [
            'room_number' => '112'
        ] );

        Room::create( [
            'room_number' => '156'
        ] );
    }
}
