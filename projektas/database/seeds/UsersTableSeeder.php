<?php

use Illuminate\Database\Seeder;
use App\User as User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        User::create( [
            'name' => 'Ignotas' ,
            'surname' => 'Pancerevas' ,
            'email' => 'ignotas@yahoo.com',
            'password' => bcrypt('123456'),
            'pn' => '3952503652'
        ] );

        User::create( [
            'name' => 'Karolis' ,
            'surname' => 'Banelis' ,
            'email' => 'karolis@yahoo.com',
            'password' => bcrypt('123456'),
            'pn' => '3958901203'
        ] );

        User::create( [
            'name' => 'Rufas' ,
            'surname' => 'Legeckas' ,
            'email' => 'rufas@yahoo.com',
            'password' => bcrypt('123456'),
            'pn' => '39501602315'
        ] );
    }
}
