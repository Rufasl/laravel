<?php

use Illuminate\Database\Seeder;
use App\Inventory_type as Inventory_type;

class Inventory_typesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Inventory_type::truncate();

        Inventory_type::create( [
            'type_name' => 'Stalai' ,

        ] );

        Inventory_type::create( [
            'type_name' => 'Kompiuteriai' ,
        ] );
    }
}
