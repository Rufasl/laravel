@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">

                        <html lang="lt">
                        <head>
                            <meta charset="utf-8">
                            <meta http-equiv="X-UA-Compatible" content="IE=edge">
                            <meta name="viewport" content="width=device-width, initial-scale=1">
                            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
                            <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
                            <title>Komentarai</title>
                            <!-- Bootstrap -->
                            <link href="css/bootstrap.css" rel="stylesheet">

                            <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
                            <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
                            <!--[if lt IE 9]>
                            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
                            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
                            <![endif]-->
                        </head>
                        <body>

                        <div class="container-fluid">


                            <div class="row">
                                <div class="col col-lg-10 col-md-8 col-sm-10 col-xs-12 col-lg-offset-1 col-md-offset-2 col-sm-offset-1">
                                    <div class="alert alert-info" role="alert"><strong>Įspėjimas!</strong> Parašykite komentarą.</div>

                                    <h1>Komentarai</h1>
                                    <ol class="breadcrumb">
                                        <li><a href="index.html">Pagrindinis</a></li>
                                        <li class="active">Kompiuteris</li>
                                        <li class="active">Komentarai</li>
                                    </ol>
                                    <hr>
                                    <table class="table table-striped">
                                        <tr>
                                            <th>Pavadinimas</th>
                                            <th>Pirkimo data</th>
                                            <th>Kaina, €</th>
                                            <th class="hidden-xs">Barkodas</th>
                                        </tr>
                                        <tr>
                                            <td>Kompiuteris</td>
                                            <td>2005 06 12</td>
                                            <td>1259.99</td>
                                            <td class="hidden-xs">1248756132241</td>
                                        </tr>
                                        <tr class="visible-xs">
                                            <td colspan="3">
                                                <table class="table">
                                                    <tr>
                                                        <th>Barkodas</th>
                                                        <td>1248756132241</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <br>

                                    <div class="form-group">
                                        <label for="comment">Komentaras:</label>
                                        <textarea class="form-control" rows="5" id="comment"></textarea>
                                    </div>
                                    <button type="submit" class="btn btn-default">Komentuoti</button>
                                    <button type="submit" class="btn btn-default">Peržiūrėti</button>
                                    </form>
                                    <br>
                                    <br>
                                    <br>
                                    <br>

                                    <ul class="media-list">
                                        <li class="media">
                                            <div class="media-left">
                                                <a href="#">
                                                    <img class="media-object" width="42" height="42"
                                                         src="http://www.hbc333.com/data/out/190/47199326-profile-pictures.png">
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading">Juozas Petrauskas</h4>
                                                Sugedo kompiuteris, nepasispaudžia ž raidė
                                                <div align="right">
                                                    <button type="button" class="btn btn-default" >
                                                        <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                                                    </button>
                                                    <button type="button" class="btn btn-default" >
                                                        <span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span>
                                                    </button>
                                                    <button type="button" class="btn btn-default" >
                                                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                                    </button>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>

                                </div>
                            </div>
                        </body>
                        </html>



                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
