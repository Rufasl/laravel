<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

        <title>Inventoriaus valdymo posistemė</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #E7E7E7;
                font-family: 'Raleway', sans-serif;
                background-image: url({{ asset('img/back.jpg') }});
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                /*color: #636b6f;*/
                color: #ffffff;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @if (Auth::check())
                        <a href="{{ url('/home') }}">Pagrindinis</a>
                        <a href="{{ url('/logout') }}">Atsijungti</a>
                    @else
                        <a href="{{ url('/login') }}">Prisijungti</a>
                        <a href="{{ url('/register') }}">Registracija</a>
                    @endif
                </div>
            @endif

            <div class="content">

                     <div class="title m-b-md visible-lg-inline">
                    <span>Inventoriaus valdymo posistemė</span>
                     </div>

                    <div>
                     <font size="20" class="hidden-lg">Inventoriaus valdymo posistemė</font>
                   </div>

                <div class="links visible-lg-inline">
                    <a href="/inventorius">Inventorius</a>
                    <a href="/informacija">Informacija</a>
                    <a href="/komentarai">Komentarai</a>
                </div>
<br>
                <div class="links hidden-lg">
                  <a href="/inventorius">
                  <button type="button" class="btn btn-default btn-lg">
                      <span class="glyphicon glyphicon-lock" aria-hidden="true"></span> Inventorius</a>
                    </button><br>
                    <a href="/informacija">
                    <button type="button" class="btn btn-default btn-lg">
                        <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> Informacija</a>
                      </button><br>
                      <a href="/komentarai">
                      <button type="button" class="btn btn-default btn-lg">
                          <span class="glyphicon glyphicon-comment" aria-hidden="true"></span> Komentarai</a>
                        </button><br>
                </div>

            </div>

        </div>
    </body>
</html>
