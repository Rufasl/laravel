@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>
                    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
                    <div class="panel-body">
                        <html lang="lt">
                        <head>
                            <meta charset="utf-8">
                            <meta http-equiv="X-UA-Compatible" content="IE=edge">
                            <meta name="viewport" content="width=device-width, initial-scale=1">
                            <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
                            <title>Inventorius</title>
                            <!-- Bootstrap -->
                            <link href="css/bootstrap.css" rel="stylesheet">


                            <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
                            <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
                            <!--[if lt IE 9]>
                            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
                            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
                            <![endif]-->

                        </head>
                        <body>

                        <div class="container-fluid">
                            <div class="row">
                                <div class="col col-lg-10 col-md-8 col-sm-10 col-xs-12 col-lg-offset-1 col-md-offset-2 col-sm-offset-1">
                                    <h1>Išsami inventoriaus informacija</h1>
                                    <ol class="breadcrumb">
                                        <li><a href="index.html">Pagrindinis</a></li>
                                        <li class="active">Kompiuteris</li>
                                        <li class="active">Informacija</li>
                                    </ol>
                                    <hr>
                                    <table class="table table-striped">
                                        <tr>
                                            <th>Pavadinimas</th>
                                            <th>Pirkimo data</th>
                                            <th>Kaina, €</th>
                                            <th class="hidden-xs">Barkodas</th>
                                            <th class="visible-lg">Atsakingas asmuo</th>
                                        </tr>
                                        <tr>
                                            <td>Kompiuteris</td>
                                            <td>2005 06 12</td>
                                            <td>1259.99</td>
                                            <td class="hidden-xs">1248756132241</td>
                                            <td class="visible-lg">Ignotas Pancerevas</td>
                                        </tr>
                                        <tr>
                                          <table class="table">
                                            <tr  class="visible-xs">
                                              <th>Barkodas</th>
                                              <td>1248756132241</td>
                                            </tr>
                                            <tr class="hidden-lg">
                                            <th>Atsakingas asmuo</th>
                                            <td>Ignotas Pancerevas</td>
                                          </tr>
                                          </table>
                                        </tr>
                                    </table>
                                    <br>
                                    <br>
                                    <h3>Atsakingų asmenų istorija</h3>
                                    <table class="table table-striped">
                                        <tr>
                                            <th>Vardas</th>
                                            <th>Pavardė</th>
                                            <th>Data nuo</th>
                                            <th>Data iki</th>
                                        </tr>
                                        <tr>
                                            <td>Karolis</td>
                                            <td>Banelis</td>
                                            <td>2016-12-28</td>
                                            <td>2017-01-12</td>
                                        </tr>
                                        <tr>
                                            <td>Rufas</td>
                                            <td>Legeckas</td>
                                            <td>2017-01-13</td>
                                            <td>2017-02-30</td>
                                        </tr>
                                    </table>
                                    <br>

                                    <ul class="nav nav-tabs">
                                        <li role="presentation" class="active"><a href="#">Inventoriaus aprašymas</a></li>
                                        <li role="presentation"><a href="#">Detali informacija</a></li>
                                    </ul>
                                    <br>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Aprašymas</h3>
                                        </div>
                                        <div class="panel-body">
                                            Patogaus bei elegantiško dizaino kompiuteris LENOVO B50-80 Pentium yra puikus pirkinys. Nešiojamas kompiuteris puikiai tinka mokslui bei  darbui. Pilnavertė skaičių sistema esanti kompiuteryje užtikrins produktyvų bei greitą darbą. Kompiuteryje jau yra įdiegta operacinė sistema Windows 10.
                                        </div>
                                    </div>

                                </div>
                        </body>
                        </html>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
