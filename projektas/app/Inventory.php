<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
   // use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id',
    ];
}
