<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('game_id')->unsgined()->index();
            $table->tinyInteger('x')->unsgined()->index();
            $table->tinyInteger('y')->unsgined()->index();
            $table->tinyInteger('mines')->unsigned();
            $table->boolean('mine')->unsigned();
            $table->boolean('covered')->unsigned();
            $table->boolean('marked')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tiles');
    }
}
