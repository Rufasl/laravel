<?php

use Illuminate\Database\Seeder;

use App\Status as Status;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Status::truncate();

        Status::create([
            'name' => 'In progress',
        ]);

        Status::create([
            'name' => 'Win',
        ]);

        Status::create([
            'name' => 'Lose',
        ]);
    }
}
