<?php

use Illuminate\Database\Seeder;

use App\Template as Template;

class TemplatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Template::truncate();

        Template::create([
            'name' => 'Beginner',
            'width' => 10,
            'height' => 10,
            'mines' => 25,
        ]);

        Template::create([
            'name' => 'Intermediate',
            'width' => 20,
            'height' => 10,
            'mines' => 50,
        ]);

        Template::create([
            'name' => 'Expert',
            'width' => 20,
            'height' => 20,
            'mines' => 100,
        ]);

        Template::create([
            'name' => 'Custom',
            'width' => 30,
            'height' => 30,
            'mines' => 150,
        ]);
    }
}
