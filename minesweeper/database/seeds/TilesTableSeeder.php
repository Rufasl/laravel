<?php

use Illuminate\Database\Seeder;

use App\Tile as Tile;

class TilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tile::truncate();
    }
}
