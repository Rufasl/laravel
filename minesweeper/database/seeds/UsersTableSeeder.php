<?php

use Illuminate\Database\Seeder;

use App\User as User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        User::create([
            'name' => 'Saulius' ,
            'email' => 'saulsink@gmail.com',
            'password' => bcrypt('test')
        ]);

        User::create([
            'name' => 'Nemo' ,
            'email' => 'nemo@gmail.com',
            'password' => bcrypt('nemo')
        ]);
    }
}
