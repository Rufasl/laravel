<?php

use Illuminate\Database\Seeder;

use App\Game as Game;

class GamesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Game::truncate();
    }
}
