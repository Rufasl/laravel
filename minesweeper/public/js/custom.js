$(document).ready(function () {

    function mytoggle(id)
    {
        $(".menu-toggles").find('i:first').removeClass('fa-minus').addClass('fa-plus');

        var iSelector = $("#menu-toggle" + id).find('i:first');

        if ($("#collapseTable" + id).hasClass('in'))
        {
            iSelector.removeClass('fa-minus').addClass('fa-plus');
        }
        else
        {
            iSelector.removeClass('fa-plus').addClass('fa-minus');
        }
    }

    $('.collapse').on('show.bs.collapse', function () {
        $('.collapse.in').collapse('hide');
    });

});