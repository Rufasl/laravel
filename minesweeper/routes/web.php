<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

// User
Auth::routes();

// Home
Route::get('/', 'HomeController@index');

// Game
Route::get('/game', 'GameController@index');
Route::get('/game/create', 'GameController@create');
Route::post('/game/store', 'GameController@store');
Route::get('/game/{id}', 'GameController@show');
Route::post('/game/{id}', 'GameController@update');
Route::get('/game/load_board/{id}', 'GameController@load_board');
Route::get('/game/load_game_status/{id}', 'GameController@load_game_status');
