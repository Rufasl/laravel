@extends('layouts.app')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Game board
            <div class="form-group pull-right">
                <label class="radio-inline">
                    <input type="radio" name="action" id="action" value="1" class="visible-xs visible-sm"
                           @if (Session::get('action') != 2) checked @endif> <span
                            class="visible-xs">&nbsp;</span><span
                            class="visible-sm">Show</span>
                </label>
                <label class="radio-inline">
                    <input type="radio" name="action" id="action" value="2" class="visible-xs visible-sm"
                           @if (Session::get('action') == 2) checked @endif> <span
                            class="visible-xs">&nbsp;</span><span
                            class="visible-sm">(Un)mark</span>
                </label>
                <button type="button" class="btn btn-default btn-xs" aria-label="Zoom in" id="zoom-in">
                    <span class="glyphicon glyphicon-zoom-in" aria-hidden="true"></span>
                </button>
                <button type="button" class="btn btn-default btn-xs" aria-label="Zoom out" id="zoom-out">
                    <span class="glyphicon glyphicon-zoom-out" aria-hidden="true"></span>
                </button>
            </div>
            <div id="loading" style="display: none" class="pull-right">
                <i class="fa fa-spinner fa-spin" style="font-size:24px"></i> loading&nbsp;
            </div>
        </div>
        <div class="panel-body">

            <div id="progress"></div>
            <div id="mines"></div>
            <div id="message"></div>

            <table class="table-bordered" id="board">
                @for($i = 0; $i < $game->width; $i++)
                    <tr>
                        @for($j = 0; $j < $game->height; $j++)
                            <td id="td{{ $i }}_{{ $j }}" class="fixed"></td>
                        @endfor
                    </tr>
                @endfor
            </table>

        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function () {

            function load_board() {
                $.get("{{ url('/game/load_board/' . $game->id) }}", function (data) {
                    var tiles = jQuery.parseJSON(data);

                    for (var x in tiles) {
                        var id = '#' + tiles[x].x + '_' + tiles[x].y;
                        var tdid = '#td' + tiles[x].x + '_' + tiles[x].y;

                        if (tiles[x].covered == '0' && tiles[x].mine == '1') {
                            //$('input[value="' + tiles[x].x + ':' + tiles[x].y + '"]').val('a');
                            //var value = $( this ).val();
                            //$( "p" ).text( value );
                            //alert('#'+ tiles[x].x + '_' + tiles[x].y);
                            $(tdid).html('<span class="glyphicon glyphicon-fire" aria-hidden="true"></span>');
                            $(tdid).css("background-color", "red");
                        }
                        else if (tiles[x].marked == '1') {
                            $(tdid).html('<span class="glyphicon glyphicon-flag" aria-hidden="true"></span>');
                            $(tdid).css("background-color", "gold");
                        }
                        else if (tiles[x].covered == '0') {
                            if (tiles[x].mines > 0)
                                $(tdid).text(tiles[x].mines);
                            $(tdid).css("background-color", "white");
                        }
                        else {
                            $(tdid).text("");
                            $(tdid).css("background-color", "gray");
                        }
                    }
                });
            }

            function load_game_status() {
                $.get("{{ url('/game/load_game_status/' . $game->id) }}", function (data) {
                    var mines = jQuery.parseJSON(data);
                    $("#mines").html(mines.marked + " mines of " + mines.total + " are marked");
                    $("#progress").html("Game status: " + mines.status);
                });
            }

            load_game_status();

            var loading = $("#loading");

            $(document).ajaxStart(function () {
                loading.show();
            });

            $(document).ajaxStop(function () {
                loading.hide();
            });

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $("#zoom-in").click(function (e) {
                $("#board tr td").css("width", $("table tr td")[0].getBoundingClientRect().width + 5);
                $("#board tr td").css("height", $("table tr td")[0].getBoundingClientRect().height + 5);
                $("#board tr td").css("font-size", parseInt($("table tr td").css("font-size")) + 3);
            });

            $("#zoom-out").click(function (e) {
                //alert(-1);
                $("#board tr td").css("width", $("table tr td")[0].getBoundingClientRect().width - 5);
                $("#board tr td").css("height", $("table tr td")[0].getBoundingClientRect().height - 5);
                $("#board tr td").css("font-size", parseInt($("table tr td").css("font-size")) - 3);
            });

            load_board();

            $(document).on("contextmenu", function (e) {
                if (e.target.nodeName == "TD" || e.target.nodeName == "SPAN")
                    e.preventDefault();
            });

            $('#board tbody tr td').mousedown(function (e) {
                        if (e.button == 0) {
                            var posting = $.post("{{ url('/game/' . $game->id) }}", {
                                tile: $(this).attr('id'),
                                action: $('input[name=action]:checked').val()
                            });

                            posting.done(function (data) {
                                $("#message").html("Last message: " + data);
                                load_board();
                                load_game_status();
                            });

                            posting.fail(function (xhr, status, error) {
                                alert("Request failed: " + status + " " + error);
                            });
                        }

                        if (e.button == 2) {
                            var posting = $.post("{{ url('/game/' . $game->id) }}", {tile: $(this).attr('id'), action: 2});

                            posting.done(function (data) {
                                $("#message").html("Last message: " + data);
                                load_board();
                                load_game_status();
                            });

                            posting.fail(function (xhr, status, error) {
                                alert("Request failed: " + status + " " + error);
                            });
                        }
                    });
        })
    </script>
@endsection