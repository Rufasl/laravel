@extends('layouts.app')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Games</div>
        <div class="panel-body">

            <form class="form-inline">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="any search phrase" name="search"
                           value="{{ $search }}">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-default btn-group-justified"><span
                                class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                </div>
            </form>

            <div class="row">&nbsp;</div>

            @include('common.alerts')

            <table class="table table-hover">
                <thead>
                <tr>
                    <th>Created at</th>
                    <th>Status</th>
                    <th class="hidden-xs">Width</th>
                    <th class="hidden-xs">Height</th>
                    <th class="hidden-xs">Mines</th>
                    <th class="hidden-xs"></th>
                    <th class="visible-xs">Expand</th>
                </tr>
                </thead>
                <tbody>
                @if (count($data) > 0)
                    @foreach ($data as $key => $object)
                        <tr class="accordion-toggle">
                            <td>{{ $object->created_at }}</td>
                            <td class="@if($object->status_id == 1) text-info @elseif($object->status_id == 2) text-success @elseif($object->status_id == 3) text-danger @endif">{{ $object->name }}</td>
                            <td class="hidden-xs">{{ $object->width }}</td>
                            <td class="hidden-xs">{{ $object->height }}</td>
                            <td class="hidden-xs">{{ $object->mines }}</td>
                            <td class="hidden-xs">
                                @if ($object->status_id == 1)
                                    <a href="{{ url('/game/' . $object->id) }}" class="btn btn-success btn-sm"><span
                                            class="glyphicon glyphicon-play" aria-hidden="true"></span></a>
                                @else
                                    <a href="{{ url('/game/' . $object->id) }}" class="btn btn-default btn-sm"><span
                                                class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>
                                @endif
                            </td>
                            <td class="visible-xs">
                                <a onclick="mytoggle({{ $object->id }})" id="menu-toggle{{ $object->id }}" class="menu-toggles" href="#" data-toggle="collapse" data-target="#collapseTable{{ $object->id }}"><i class="fa fa-plus btn-lg"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="7" class="hiddenRow">
                                <div id="collapseTable{{ $object->id }}" class="collapse">
                                    <table class="table table-striped table-hover table-condensed">
                                        <tr class="visible-xs">
                                            <th>Width</th>
                                            <td>{{ $object->width }}</td>
                                        </tr>
                                        <tr class="visible-xs">
                                            <th>Height</th>
                                            <td>{{ $object->height }}</td>
                                        </tr>
                                        <tr class="visible-xs">
                                            <th>Mines</th>
                                            <td>{{ $object->mines }}</td>
                                        </tr>
                                        <tr class="visible-xs">
                                            <th>Play/View</th>
                                            <td><a href="{{ url('/game/' . $object->id) }}"
                                                   class="btn btn-success btn-sm">@if ($object->status_id == 1)<span
                                                            class="glyphicon glyphicon-play" aria-hidden="true"></span> @else <span
                                                            class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> @endif</a></td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="6">No results!</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>

        <div class="panel-footer">
            <div class="@if($data->total() > 1) visible-xs @else hidden-xs @endif">
                <div class="btn-group btn-group-justified">
                    <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            Page: {{ $data->currentPage() }} of {{ $data->lastPage() }} <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li @if ($data->currentPage() == 1) class="disabled" @endif><a
                                        href="{{ $data->previousPageUrl() }}">Previous</a></li>
                            <li @if ($data->hasMorePages() == false) class="disabled" @endif><a
                                        href="{{ $data->nextPageUrl() }}">Next</a></li>
                            <li @if ($data->currentPage() == 1) class="disabled" @endif><a
                                        href="{{ $data->url($data->url(1)) }}">First</a></li>
                            <li @if ($data->currentPage() == $data->url($data->lastPage())) class="disabled" @endif><a
                                        href="{{ $data->url($data->lastPage()) }}">Last</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="hidden-xs">{{ $data->links() }}</div>
            <div class="@if($data->total() == 1) visible-xs @else hidden-xs @endif">&nbsp;</div>
        </div>

    </div>
@endsection