@extends('layouts.app')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">New game</div>
        <div class="panel-body">

            @include('common.alerts')

            <form action="{{ url('/game/store') }}" method="post">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="template_id">Template</label>
                    <div class="radio">
                        <label>
                            <input type="radio" name="template_id" id="template_id" value="1"
                                   @if(old('template_id') == 1 || old('template_id') == '') checked
                                   @endif data-toggle="collapse" data-target=".collapseOne.in">
                            Beginner
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="template_id" id="template_id" value="2"
                                   @if(old('template_id') == 2) checked @endif data-toggle="collapse"
                                   data-target=".collapseOne.in">
                            Intermediate
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="template_id" id="template_id" value="3"
                                   @if(old('template_id') == 3) checked @endif data-toggle="collapse"
                                   data-target=".collapseOne.in">
                            Expert
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="template_id" id="template_id" value="4"
                                   @if(old('template_id') == 4) checked @endif data-toggle="collapse"
                                   data-target=".collapseOne:not(.in)">
                            Custom
                        </label>
                    </div>
                </div>
                <div class="collapseOne panel-collapse collapse @if (old('template_id') == 4) in @endif">
                    <div class="form-group">
                        <label for="width">Width</label>
                        <input type="text" class="form-control" id="width" name="width" value="{{ old('width') }}">
                    </div>
                    <div class="form-group">
                        <label for="height">Height</label>
                        <input type="text" class="form-control" id="height" name="height" value="{{ old('height') }}">
                    </div>
                    <div class="form-group">
                        <label for="mines">Mines</label>
                        <input type="text" class="form-control" id="mines" name="mines" value="{{ old('mines') }}">
                    </div>
                </div>
                <button type="submit" class="btn btn-success">Save</button>
                <a href="{{ url('/game') }}" class="btn btn-default">Back to games</a>
            </form>

        </div>
    </div>
@endsection