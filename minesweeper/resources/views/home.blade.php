@extends('layouts.app')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Home</div>

        <div class="panel-body">

            <table class="table table-striped">
                <tbody>
                <tr>
                    <th>Pavadinimas</th>
                    <th>Kabinetas</th>
                    <th>Kaina, €</th>
                    <th>Kiekis, vnt.</th>
                    <th></th>
                </tr>
                <tr>
                    <td>Stalas</td>
                    <td>112</td>
                    <td>29.99</td>
                    <td>1</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Kompiuteris <span class="label label-default">New</span></td>
                    <td>156</td>
                    <td>1259.99</td>
                    <td>1</td>
                    <td  class="hidden-xs">
                        <!--  <a href="komentarai.html" span class="glyphicon glyphicon-comment" aria-hidden="true"></a>
                          <a href="informacija.html" span class="glyphicon glyphicon-info-sign" aria-hidden="true"></a> -->
                        <!-- Single button -->
                        <div class="btn-group">
                            <button type="button" class="btn btn-warning btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Daugiau <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="informacija.html" span class="glyphicon glyphicon-info-sign" aria-hidden="true"> Informacija</a></li>
                                <li><a href="komentarai.html" span class="glyphicon glyphicon-comment" aria-hidden="true"> Komentarai</a></li>
                            </ul>
                        </div>
                </tr>
                <tr class="visible-xs">
                    <td colspan="3">
                        <table class="table">
                            <tr>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-warning btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Daugiau <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a href="informacija.html" span class="glyphicon glyphicon-info-sign" aria-hidden="true"> Informacija</a></li>
                                        <li><a href="komentarai.html" span class="glyphicon glyphicon-comment" aria-hidden="true"> Komentarai</a></li>
                                    </ul>
                                </div>
                            </tr>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
