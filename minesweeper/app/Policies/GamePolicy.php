<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

use App\Game;

class GamePolicy
{
    use HandlesAuthorization;

    public function show(User $user, Game $game)
    {
        return $user->id == $game->user_id;
    }
}
