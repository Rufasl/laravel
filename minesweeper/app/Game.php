<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    public function tiles()
    {
        return $this->hasMany('App\Tile');
    }

    public function status()
    {
        return $this->belongsTo('App\Status');
    }
}
