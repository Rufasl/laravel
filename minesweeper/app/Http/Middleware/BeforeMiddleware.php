<?php

namespace App\Http\Middleware;

use Closure;
use DB;

class BeforeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        DB::enableQueryLog();
        return $next($request);
    }

    public function terminate($request, $response)
    {
        // Store or dump the log data...
        /*echo '<pre>';
        var_dump(DB::getQueryLog());
        echo '</pre>';*/

        //echo "<div style='margin-top:10px'></div>";

        //dd(DB::getQueryLog());
    }
}
