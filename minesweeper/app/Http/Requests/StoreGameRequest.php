<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class StoreGameRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $min_mines = (int) ($request->input('width') * $request->input('height') * 0.05);
        $max_mines = (int) ($request->input('width') * $request->input('height') * 0.25);

        return [
            'template_id' => 'required|integer|exists:templates,id',
            'width' => 'required_if:template_id,4|integer|between:4,30',
            'height' => 'required_if:template_id,4|integer|between:4,30',
            'mines' => sprintf('bail|required_if:template_id,4|integer|min:%d|max:%d', $min_mines, $max_mines),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'width.required_if' => 'Width is required then template is Custom.',
            'height.required_if' => 'Height is required then template is Custom.',
            'mines.required_if' => 'Mines is required then template is Custom.',
        ];
    }
}
