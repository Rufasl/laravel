<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['total_users'] = DB::table('users')->count();
        $data['total_games'] = DB::table('games')->count();
        $data['total_in_progress'] = DB::table('games')->where('status_id', 1)->count();
        $data['total_wins'] = DB::table('games')->where('status_id', 2)->count();
        $data['total_loses'] = DB::table('games')->where('status_id', 3)->count();

        $data['best'] = DB::table('games')
            ->select(DB::raw('count(games.id) as count, users.name'))
            ->leftJoin('users', 'users.id', '=', 'games.user_id')
            ->where('status_id',2)
            ->groupBy('user_id')
            ->first();

        return view('home')->with(compact('data'));
    }
}
