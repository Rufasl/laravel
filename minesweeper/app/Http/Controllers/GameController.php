<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\StoreGameRequest;

use App\Game;
use App\Tile;
use App\Template;
use App\Status;
use Auth;
use DB;

class GameController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = DB::table('games')
            ->select('games.*', 'statuses.name')
            ->leftJoin('statuses', 'statuses.id', '=', 'games.status_id')
            ->where('user_id', Auth::user()->id)
            ->orderBy('games.created_at', 'desc');

        $search = $request->input('search');
        if ($search != '')
        {
            $pattern = '%' . $search . '%';
            $data->where('mines', 'LIKE', $pattern);
            $data->orWhere('width', 'LIKE', $pattern);
            $data->orWhere('height', 'LIKE', $pattern);
            $data->orWhere('games.created_at', 'LIKE', $pattern);
            $data->orWhere('statuses.name', 'LIKE', $pattern);
        }

        $data = $data->paginate(10);

        return view('game/index')->with(compact('data', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('game/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreGameRequest $request)
    {
        // get template

        $template = Template::findOrFail($request->input('template_id'));

        // create new game by template or custom

        $game = new Game();
        $game->user_id = Auth::user()->id;
        $game->status_id = 1;

        if ($template->id == 4) {
            $game->width = $request->input('width');
            $game->height = $request->input('height');
            $game->mines = $request->input('mines');
        } else {
            $game->width = $template->width;
            $game->height = $template->height;
            $game->mines = $template->mines;
        }

        $game->save();

        // create all tiles

        $tiles = array(array());
        for ($i = 0; $i < $game->width; $i++) {
            for ($j = 0; $j < $game->height; $j++) {
                $tile = new Tile();
                $tile->game_id = $game->id;
                $tile->x = $i;
                $tile->y = $j;
                $tile->covered = 1;
                $tile->marked = 0;
                $tile->mines = 0;
                $tile->mine = 0;

                $tiles[$i][$j] = $tile;
            }
        }

        // place all mines randomly

        $placed = 0;
        while ($placed < $game->mines) {
            $x = rand(0, $game->width - 1);
            $y = rand(0, $game->height - 1);

            if ($tiles[$x][$y]->mine == 0) {
                $tiles[$x][$y]->mine = 1;
                $placed++;
            }
        }

        // calculate adjacent mines for each tile

        for ($i = 0; $i < $game->width; $i++) {
            for ($j = 0; $j < $game->height; $j++) {
                $mines = 0;
                if (isset($tiles[$i - 1][$j - 1]) && $tiles[$i - 1][$j - 1]->mine == 1)
                    $mines++;
                if (isset($tiles[$i][$j - 1]) && $tiles[$i][$j - 1]->mine == 1)
                    $mines++;
                if (isset($tiles[$i + 1][$j - 1]) && $tiles[$i + 1][$j - 1]->mine == 1)
                    $mines++;
                if (isset($tiles[$i - 1][$j]) && $tiles[$i - 1][$j]->mine == 1)
                    $mines++;
                if (isset($tiles[$i + 1][$j]) && $tiles[$i + 1][$j]->mine == 1)
                    $mines++;
                if (isset($tiles[$i - 1][$j + 1]) && $tiles[$i - 1][$j + 1]->mine == 1)
                    $mines++;
                if (isset($tiles[$i][$j + 1]) && $tiles[$i][$j + 1]->mine == 1)
                    $mines++;
                if (isset($tiles[$i + 1][$j + 1]) && $tiles[$i + 1][$j + 1]->mine == 1)
                    $mines++;

                $tiles[$i][$j]->mines = $mines;
            }
        }

        // make one array for all tiles
        $array = array();
        for ($i = 0; $i < $game->width; $i++) {
            for ($j = 0; $j < $game->height; $j++) {
                $array[] = $tiles[$i][$j]->toArray();
            }
        }

        // one insert for all tiles
        Tile::insert($array);

        return redirect('/game/' . $game->id)->with('alert-success', 'New game was created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $game = Game::findOrFail($id);

        $this->authorize('show', $game);

        $tiles = Tile::where('game_id', '=', $id)->get();
        $data = array(array());
        $tile = 0;
        for ($i = 0; $i < $game->width; $i++) {
            for ($j = 0; $j < $game->height; $j++) {
                $data[$i][$j] = $tiles[$tile];
                $tile++;
            }
        }

        return view('game/show')->with(compact('data', 'game'));
    }

    public function load_board($id)
    {
        $game = Game::findOrFail($id);

        $this->authorize('show', $game);

        $tiles = Tile::where('game_id', '=', $id)->get();

        echo json_encode($tiles);
    }

    public function load_game_status($id)
    {
        $game = Game::with('status')->findOrFail($id);

        $this->authorize('show', $game);

        $marked = Tile::where('game_id', '=', $id)->where('marked', '=', 1)->count();
        $total = Game::findOrFail($id)->mines;

        echo json_encode(array('marked' => $marked, 'total' => $total, 'status' => $game->status->name));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    private function uncover_zeroes($id, $x, $y)
    {
        $combinations = [[-1, -1], [0, -1], [+1, -1], [-1, 0], [+1, 0], [-1, +1], [0, +1], [+1, +1]];

        foreach ($combinations as $xy)
        {
            $tile = Tile::where('game_id', '=', $id)->where('x', '=', $x + $xy[0])->where('y', '=', $y + $xy[1])->first();
            if (!is_null($tile) && $tile->covered == 1) {
                $tile->covered = 0;
                $tile->save();
                if ($tile->mines == 0)
                    $this->uncover_zeroes($id, $x + $xy[0], $y + $xy[1]);
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $game = Game::findOrFail($id);

        $this->authorize('show', $game);

        $action = $request->input('action');

        $coords = explode('_', $request->input('tile'));
        $x = substr($coords[0], 2);
        $y = $coords[1];

        $tile = Tile::where('game_id', '=', $id)->where('x', '=', $x)->where('y', '=', $y)->firstOrFail();

        if ($tile->covered == 1)
        {
            if ($action == 1) // uncover
            {
                $tile->covered = 0;
                $tile->save();

                if ($tile->mine == 1)
                {
                    $game = Game::findOrFail($id);
                    $game->status_id = Status::where('name', 'Lose')->firstOrFail()->id;
                    $game->save();

                    $message = 'Lose!!!';
                }
                else
                    $message = 'Uncovered';

                if ($tile->mines == 0)
                {
                    $this->uncover_zeroes($id, $x, $y);
                }
            }
            elseif ($action == 2) // (un)mark
            {
                if ($tile->marked == 1)
                {
                    $tile->marked = 0;

                    $message = 'Unmarked';
                }
                else
                {
                    $tile->marked = 1;

                    $message = 'Marked';
                }
                $tile->save();
            }
        }
        else
            $message = 'No action';

        // check if winner
        $uncovered = Tile::where('game_id', $id)->where('covered', 0)->count();
        $game = Game::findOrFail($id);
        if ($uncovered == $game->width * $game->height - $game->mines)
        {
            $game->status_id = 2;
            $game->save();
            $message = 'Win!';
        }

        echo $message;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
